﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace io.newgrounds.samples
{
    [System.Serializable] public class UnityStringEvent : UnityEvent<string> { }

    /// <summary> This posts the elapsed game time to the scoreboard. </summary>
    [RequireComponent(typeof(Button))]
    public class TimePoster : MonoBehaviour
    {
        [SerializeField, Tooltip("The ID of the scoreboard as found in your API Tools.")]
        int scoreboardId = -1;

        Button postButton;
        NGHelper_Scoreboard ngHelper;

        // We can link any MonoBehaviour's public function from within the editor
        [Space] 
        public UnityStringEvent onScorePosted = default; // do something after the score is posted

        void Awake()
        {
            postButton = GetComponent<Button>();
        }

        void Start()
        {
            ngHelper = FindObjectOfType<NGHelper_Scoreboard>();
            if (ngHelper == null)
            {
                Debug.LogError("There is no NGHelper in the current scene, so it's not possible to post scores.");
                enabled = false;
            }
            else
            {
                postButton.onClick.AddListener(PostElapsedTime);
            }
        }

        void PostElapsedTime()
        {
            int secondsSinceStart = Mathf.FloorToInt(Time.timeSinceLevelLoad * 1000); // milliseconds
            ngHelper.postScore(scoreboardId, secondsSinceStart, onScorePosted); // Display the current score
        }
    }
}
