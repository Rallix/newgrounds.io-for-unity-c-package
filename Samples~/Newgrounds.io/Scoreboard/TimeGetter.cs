﻿using UnityEngine;
using UnityEngine.UI;

namespace io.newgrounds.samples
{
    /// <summary> This posts the elapsed game time to the scoreboard. </summary>
    [RequireComponent(typeof(Button))]
    public class TimeGetter : MonoBehaviour
    {
        [SerializeField, Tooltip("The ID of the scoreboard as found in your API Tools.")]
        int scoreboardId = -1;

        Button getButton;
        NGHelper_Scoreboard ngHelper;

        [Space] public UnityStringEvent onScoresGet = default; // do something after the score is gathered

        void Awake()
        {
            getButton = GetComponent<Button>();
        }

        void Start()
        {
            ngHelper = FindObjectOfType<NGHelper_Scoreboard>();
            if (ngHelper == null)
            {
                Debug.LogError("There is no NGHelper in the current scene, so it's not possible to post scores.");
                enabled = false;
            }
            else
            {
                getButton.onClick.AddListener(GetTimeScores);
            }
        }

        void GetTimeScores()
        {
            ngHelper.getScores(scoreboardId, onScoresGet);
        }
    }
}
