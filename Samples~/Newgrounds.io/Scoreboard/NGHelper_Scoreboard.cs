﻿using System.Linq;
using io.newgrounds.objects;
using UnityEngine;
using UnityEngine.Events;

namespace io.newgrounds.samples
{
    [RequireComponent(typeof(core))]
    public class NGHelper_Scoreboard : MonoBehaviour
    {
        io.newgrounds.core ngio_core;

        // User currently playing the game
        objects.user player;

        [Space] public UnityEvent onLoggedInEvent = new UnityEvent();

        void Awake()
        {
            ngio_core = GetComponent<core>();

            // Check if the user has a saved login when your game starts
            // Wait until the core has been initialized
            ngio_core.onReady(() =>
            {
                // Call the server to check login status
                ngio_core.checkLogin(logged_in =>
                {
                    if (logged_in)
                    {
                        onLoggedIn();
                    }
                    else
                    {
                        // Opens up Newgrounds Passport if they are not logged in
                        requestLogin();
                    }
                });
            });
        }

        // Gets called when the player is signed in.
        void onLoggedIn()
        {
            // Do something. You can access the player's info with:
            player = ngio_core.current_user;
            onLoggedInEvent?.Invoke(); // invoke the logged in event (if there is one)
        }

        // displays the message
        public void onLoggedIn_message()
        {
            Debug.Log("You are successfully logged in.");
        }

        // Gets called if there was a problem with the login (expired sessions, server problems, etc).
        void onLoginFailed()
        {
            // Do something. You can access the login error with:
            objects.error error = ngio_core.login_error;
            Debug.Log($"<b>Login failed</b>: {error.message}", gameObject);
        }

        // Gets called if the user cancels a login attempt.
        void onLoginCancelled()
        {
            // Do something...
        }

        // When the user clicks your log-in button
        void requestLogin()
        {
            // This opens passport and tells the core what to do when a definitive result comes back.
            ngio_core.requestLogin(onLoggedIn, onLoginFailed, onLoginCancelled);
        }

        /*
         * You should have a 'cancel login' button in your game and have it call this, just to be safe.
         * If the user simply closes the browser tab rather than clicking a button to cancel, we won't be able to detect that.
         */
        void cancelLogin()
        {
            /*
             * This will call onLoginCancelled because you already added it as a callback via ngio_core.requestLogin()
             * for ngio_core.onLoginCancelled()
             */
            ngio_core.cancelLoginRequest();
        }

        // And finally, have your 'sign out' button call this
        void logOut()
        {
            ngio_core.logOut();
        }

        // call this method whenever you want to unlock a medal.
        public void postScore(int scoreboard_id, int score, UnityEvent<string> onScorePostedEvent = null)
        {
            // create the component
            var post_score = new components.ScoreBoard.postScore
            {
                    // set required parameters
                    id = scoreboard_id,
                    value = score
            };

            // call the component on the server, and tell it to fire onScorePosted() when it's done.
            post_score.callWith(ngio_core, result =>
            {
                onScorePosted(result);
                onScorePostedEvent?.Invoke($"<color=orange>(+)</color> {result.score.formatted_value}");
            });

            Debug.LogFormat("Attempting to post score '{0}' to scoreboard '{1}'...", score, scoreboard_id);
        }

        public void getScores(int scoreboard_id, UnityEvent<string> onScoresGetEvent = null)
        {
            // create the component
            var get_scores = new components.ScoreBoard.getScores
            {
                    // set required parameters
                    id = scoreboard_id,
                    limit = 25,
                    // user = ngio_core.current_user,
                    period = "A",
                    social = false,
            };

            // call the component on the server, and tell it to fire onScorePosted() when it's done.
            get_scores.callWith(ngio_core, result =>
            {
                onScoresGet(result);
                string resultString = result.scores.Cast<score>().Aggregate(string.Empty, (current, score) => current + $"{score.formatted_value}\n");
                onScoresGetEvent?.Invoke(resultString);
            });

            Debug.LogFormat("Attempting to get scoreboard '{0}' data...", scoreboard_id);
        }

        // This will get called whenever a medal gets unlocked via unlockMedal()
        void onScorePosted(results.ScoreBoard.postScore result)
        {
            objects.scoreboard scoreboard = result.scoreboard;
            objects.score score = result.score;
            Debug.LogFormat("<b><color=orange>Score posted:</color></b> {0} ({1} points)", scoreboard.name, score.formatted_value);
        }

        void onScoresGet(results.ScoreBoard.getScores result)
        {
            if (!result.success) return;
            string scoresString = string.Empty;
            foreach (objects.score score in result.scores)
            {
                // this object contains all the score info, and can be cast to a proper model object.
                scoresString += $"<b>{score.user.name}</b>:\t\t\t\t{score.formatted_value}\n";
            }
            // Log scores or '<None>'
            Debug.LogFormat("<b><color=orange>Scores gathered:</color></b> {0}\n{1}", 
                            result.scoreboard.name, string.IsNullOrWhiteSpace(scoresString) ? "<color=red><None></color>" : scoresString);
        }
    }
}
