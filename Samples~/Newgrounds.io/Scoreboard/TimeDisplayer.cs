﻿using UnityEngine;
using UnityEngine.UI;

namespace io.newgrounds.samples
{
    /// <summary> Displays the given text onscreen. </summary>
    [RequireComponent(typeof(Text))]
    public class TimeDisplayer : MonoBehaviour
    {
        Text timeText; // the text which should display the elapsed time

        void Awake()
        {
            timeText = GetComponent<Text>();
        }

        // This is linked in the Editor
        // Show posted score on screen
        public void UpdateScoreText(string scoreText)
        {
            timeText.text = scoreText;
        }
    }
}
