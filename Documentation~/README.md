### Newgrounds.io

To view the full documentation, please visit the following link:

https://www.newgrounds.io/help

You can also check the **Documentation.chm** file or the visual guide by [Geckomla19097](https://geckomla19097.newgrounds.com/) in the current folder.