using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.editor
{
	/// <summary> Location of important folders in the project. </summary>
	static class NgPath
    {
        /// <summary> The default namespace of the project based on the <see cref="core"/>, most likely <see langword="io.newgrounds"/>. Also the package name.</summary>
        /// <remarks> The root namespace is also important for package management and should match the package name as specified in package.json. </remarks>
        internal static readonly string RootNamespace = $"{typeof(core).Namespace}";
		
        /// <summary> The path to the package root relative to the <see cref="ProjectRoot"/>. </summary>
        internal static readonly string PackageRoot = $"Packages/{RootNamespace}";
		
        /// <summary> The root folder of the Unity project. </summary>
		/// <remarks> <see cref="Application.dataPath"/> is the Assets folder which we remove from the path to get the project root. </remarks>
        internal static readonly string ProjectRoot = Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/'));
		
    }
}