using System.IO;
using SimpleJSON;
using UnityEditor;
using UnityEngine;

namespace io.newgrounds.editor
{
    /// <summary> All commands accessible through the top context menu. </summary>
    static class NewgroundsMenuTools
    {
        /// <summary>Base path to runnable commands via the top context menu.</summary>
        internal const string BasePath = "Window/Newgrounds/";

        /// <summary> Removes the lock from the <see langword="manifest.json"/> file, which causes the package to be updated. </summary>
        [MenuItem(BasePath + "Check for Updates", priority = 115)]
        static void CheckForUpdates()
        {
            if (!AskBeforeUpdate()) return;

            string filePath = Path.Combine(NgPath.ProjectRoot, "Packages", "manifest.json");
            if (!File.Exists(filePath))
            {
                Debug.LogError($"Can't find the package manifest:\n<i>{filePath}</i>");
                return;
            }

            JObject packageManifest = JSONDecoder.Decode(File.ReadAllText(filePath));
            const string dependencies = "dependencies";
            const string @lock = "lock";

            if (!packageManifest.ObjectValue.ContainsKey(dependencies) ||
                !packageManifest.ObjectValue[dependencies].ObjectValue.ContainsKey(NgPath.RootNamespace))
            {
                Debug.LogError($"Can't find the package <b>{NgPath.RootNamespace}</b> in the package manifest's list of dependencies.\nThis can happen if you didn't add the package via the Package Manager (i.e. embedded package).");
                return;
            }

            if (!packageManifest.ObjectValue.ContainsKey(@lock) ||
                !packageManifest.ObjectValue[@lock].ObjectValue.ContainsKey(NgPath.RootNamespace))
            {
                Debug.LogWarning($"The package lock for <b>{NgPath.RootNamespace}</b> is already missing from the manifest file.\nThis can happen if you removed it manually, or didn't install the package via git URL.");
                return;
            }

            packageManifest[@lock][NgPath.RootNamespace].ObjectValue.Clear(); // remove the actual version lock

            // apply the changes back to the file
            using (var writer = new StreamWriter(filePath))
            {
                var jsonWriter = new JSONStreamEncoder(writer);
                jsonWriter.WriteJObject(packageManifest);
            }

            AssetDatabase.Refresh(); // trigger package reimport
        }

        /// <summary> A small confirmation window before the update process proceeds.</summary>
        private static bool AskBeforeUpdate()
        {
            return EditorUtility.DisplayDialog("Newgrounds.io Package Update (git)",
                                               "This will remove the version lock of the package which will " +
                                               "force the Editor to update it to the latest version. " +
                                               "You cannot undo this action. \n\n" +
											   "Note: This is only relevant for packages installed via git URL. " +
											   "For locally installed packages, simply re-download and overwrite the current files.",
                                               "I Made a Backup. Go Ahead!", "No Thanks");
        }
    }
}
